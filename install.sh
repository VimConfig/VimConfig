#!/bin/sh

vim +PluginInstall +qall

cd ~/
ln -s VimConfig/.vim 
ln -s VimConfig/.vimrc

sudo pacman --needed -S clang cmake make python2
vim +PluginInstall +qall
cd .vim/bundle/YouCompleteMe
./install.sh

